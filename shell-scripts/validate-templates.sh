#!/bin/bash

templates=$(ls ../cloud-formation-templates/*.yml)

for template in $templates; do
    echo "validate $template"
    aws cloudformation validate-template --template-body file://"$template"
done