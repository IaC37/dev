#!/bin/bash

templates=$(ls ../cloud-formation-templates/*.yml)

for template in $templates; do
    echo "security scan $template"
    checkov --file "$template"
done