#!/bin/bash


aws sts get-caller-identity

account_name="infra"
region="us-east-1"
template_directory="../cloud-formation-templates/"
auto_deployment="Enabled=true,RetainStacksOnAccountRemoval=false"
deployment_targets="OrganizationalUnitIds=ou-g3la-rbvaxvsv"


create_stack_set()
{
    template=$template_directory$1".yml"
    stack_set_name=$2

    aws cloudformation create-stack-set \
    --stack-set-name "$stack_set_name" \
    --template-body file://"$template_directory""$template" \
    --auto-deployment "$auto_deployment" \
    --permission-model "SERVICE_MANAGED" \
    --capabilities "CAPABILITY_NAMED_IAM" \
    --call-as "DELEGATED_ADMIN" \
    --managed-execution "Active=true"

    aws cloudformation create-stack-instances \
    --stack-set-name "$stack_set_name" \
    --deployment-targets "$deployment_targets" \
    --regions "$region" \
    --call-as "DELEGATED_ADMIN"
}


update_stack_set()
{
    template=$template_directory$1".yml"
    stack_set_name=$2
    
    aws cloudformation update-stack-set \
    --stack-set-name "$stack_set_name" \
    --template-body file://"$template" \
    --capabilities CAPABILITY_NAMED_IAM \
    --call-as "DELEGATED_ADMIN"
}


templates=$(ls "$template_directory" | grep .yml | cut -d "." -f1)

for template in $templates; do
    stack_set_name=$account_name"-"$template

    aws cloudformation describe-stack-set \
    --stack-set-name "$stack_set_name" \
    --region us-east-1 \
    --call-as "DELEGATED_ADMIN" \
    &> /dev/null
        
    if [ $? -eq 0 ]
    then
        echo "update stack set"
        update_stack_set "$template" "$stack_set_name"
    else
        echo "create stack set"
        create_stack_set "$template" "$stack_set_name"
    fi
done