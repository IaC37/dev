#!/bin/bash

templates=$(ls ../cloud-formation-templates/*.yml)

for template in $templates; do
    echo "security scan $template"
    cfn_nag_scan --input-path "$template"
done