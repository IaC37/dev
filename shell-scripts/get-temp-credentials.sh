#!/bin/bash

get_temporary_credentials()
{
    echo "get temporary credentials"
    role_arn="arn:aws:iam::901203859849:role/gitlab-pipeline"

    temp_creds=(`aws sts assume-role --role-arn $role_arn --role-session-name pipeline --query '[Credentials.AccessKeyId, Credentials.SecretAccessKey, Credentials.SessionToken]' --output text`)

    export AWS_DEFAULT_REGION="us-east-1"
    export AWS_ACCESS_KEY_ID=${temp_creds[0]}
    export AWS_SECRET_ACCESS_KEY=${temp_creds[1]}
    export AWS_SESSION_TOKEN=${temp_creds[2]}
}

get_temporary_credentials