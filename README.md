# SecOps Pipeline

SecOps project to create AWS resources through Gitlab pipeline.

## Description

We may for reasons wish to restrict creation of AWS resources through the Console. This SecOps pipeline can be used to provision resources across a multitude of accounts. The pipeline has 3 stages

1. validate
2. security-scan
3. deploy

### validate

The sheel script validate-template.sh first assumes a role in the managment account. This stage validates the CloudFormation templates for syntax errors. The sheel script iterates through every template present inside the folder and checks the syntax correctness.

### security-scan

This stage performs static analysis of templates to find security issues before deployment. The following two tools are used for this purpose. You may use one as per your need.

1. Checkov
2. cfn-nag

### deploy

The final stage of the pipeline (if no errors were encountered in the previous stages) deployes the resources in the accounts present under the OU specified in the stack-set.sh file.

## Contributing

Merge Requests are most welcome. Make sure to use the latest code from the Main branch and please provide a detailed description of your changes.

## Status

README.md is incomplete. It will be updated to help you guide on how to use this project.
